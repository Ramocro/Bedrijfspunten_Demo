# Bedrijfspunten_Demo



## Opdrachtomschrijving
In deze opdracht maken we een simpele webpagina aan de hand van html, deze maken we op aan de hand van de technieken die we geleerd hebben in les 1. 
etc...

## Eisen
In dit product willen wij ten minste de volgende elementen zien:
- Een .html file
- Opmaak met 4 verschillende HTML elementen 
etc...

## Klas
Deze opdracht is bedoeld voor klas IB101, IB102 en IB103 
etc...

## Studiemateriaal
Voor deze opdracht staat lesmateriaal in de DLO course "HTML & CSS" 
- Les 1: Powerpoint
- https://www.coursera.org/learn/html-css-javascript-for-web-developers
etc...

## Authors
Ramon Mocking

## Deadline
De deadline voor deze opdracht is 1 Juli 2022
De herkansing is te maken na de zomer vakantie, neem hiervoor contact op met Jelle
etc...

## Inleveren van dit project 
Dit project wordt ingeleverd als een export van jouw repository, deze kan als zip geupload worden naar de DLO course. 
etc...
